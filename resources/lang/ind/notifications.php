<?php
return [
    'order_created'       => 'Pesanan #:id :amount :currency berhasil dibuat',
    'order_closed'        => 'Pesanan #:id :amount :currency ditutup karena kurangnya dana pada saldo',
    'sale'                => 'Penjualan :amount :currency berhasil dilakukan',
    'purchase'            => 'Pembelian :amount :currency berhasil dilakukan',
    'partner_accrue'      => 'Anda baru saja mendapatkan komisi afiliasi :amount :currency dari pengguna :login, di tingkat :level',
    'wallet_refiled'      => 'Saldo dompet Anda telah diisi dengan jumlah for :amount :currency',
    'rejected_withdrawal' => 'Penarikan Anda untuk jumlah :amount :currency telah dibatalkan.',
    'approved_withdrawal' => 'Penarikan Anda untuk jumlah :amount :currency telah dikonfirmasi.',
    'new_partner'         => 'Anda memiliki mitra yang baru :login di tingkat :level',
    'parking_bonus'       => 'Bonus Parking :amount :currency',
    'licence_cash_back'   => 'Cashback untuk pembelian lisensi :amount :currency'
];