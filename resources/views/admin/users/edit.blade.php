@extends('admin/layouts.app')

@section('title')
    {{ __('Edit user') }} {{ $user->name }}
@endsection

@section('breadcrumbs')
    <li><a href="{{route('admin.users.index')}}">{{ __('Users') }}</a></li>
    <li> @lang('Edit user'): {{ $user->name }}</li>
@endsection

@section('content')
    <!-- row -->
    <div class="row">
        <!-- col -->
        <div class="col-md-12">

            <!-- tile -->
            <section class="tile">

                <!-- tile header -->
                <div class="tile-header dvd dvd-btm">
                    <h1 class="custom-font">{{ __('Edit user') }}</h1>
                    <ul class="controls">
                        <li>
                            <a role="button" tabindex="0" class="tile-fullscreen">
                                <i class="fa fa-expand"></i> {{ __('Fullscreen') }}
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- /tile header -->

                <!-- tile body -->
                <div class="tile-body">

                    <form class="form-horizontal" method="POST" action="{{ route('admin.users.update', ['user' => $user->id]) }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="put">

                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">{{ __('User Login') }}</label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="login" value="{{ $user->login }}" required
                                       autofocus>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="email" class="col-md-4 control-label">{{ __('E-Mail Address') }}</label>
                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email"
                                       value="{{ $user->email }}" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="upline" class="col-md-4 control-label">{{ __('Upline') }}</label>
                            <div class="col-md-6">
                                <?php
                                $partner = $user->getPartnerOnLevel(1);
                                ?>
                                <input id="upline" type="text" class="form-control" name="upline" value="{{ $partner !== null ? $partner->email : '' }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password" class="col-md-4 control-label">{{ __('Password') }}</label>
                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password" class="col-md-4 control-label">{{ __('Representative') }}</label>
                            <div class="col-md-6">
                                <input id="representative" type="checkbox" value="1" name="representative"{{ $user->isRepresentative() ? ' checked' : '' }}>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label" for="currency">{{ __('Rank') }}</label>
                            <div class="col-md-4">
                                <select id="currency" name="rank_id" class="form-control">
                                    @foreach($ranks as $rank)
                                        <option value="{{ $rank->id }}"{{ $user->rank_id == $rank->id ? ' selected' : '' }}>{{ $rank->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label" for="selectmultiple">{{ __('Roles') }}</label>
                            <div class="col-md-4">
                                <select id="selectmultiple" name="roles[]" class="form-control" multiple="multiple">
                                    @foreach($roles as $role)
                                        @if ($user->hasRole($role['name']))
                                            <option value="{{ $role['name'] }}" selected>{{ $role['name'] }}</option>
                                        @else
                                            <option value="{{ $role['name'] }}">{{ $role['name'] }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="sell_limit" class="col-md-4 control-label">{{ __('Sell Limit') }}</label>
                            <div class="col-md-6">
                                <input id="sell_limit" type="text" class="form-control" name="sell_limit" value="{{ $user->sell_limit }}" required
                                       autofocus>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="buy_limit" class="col-md-4 control-label">{{ __('Buy Limit') }}</label>
                            <div class="col-md-6">
                                <input id="buy_limit" type="text" class="form-control" name="buy_limit" value="{{ $user->buy_limit }}" required
                                       autofocus>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Update') }}
                                </button>
                            </div>
                        </div>
                    </form>

                </div>
                <!-- /tile body -->

            </section>
            <!-- /tile -->

        </div>
        <!-- /col -->
    </div>
    <!-- /row -->


@endsection