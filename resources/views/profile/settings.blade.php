@extends('layouts.profile')
@section('title', __('Settings'))
@section('content')


    <section class="section-border">

        <meta name="csrf-token" content="{{ csrf_token() }}"/>


        <div class="container">
            <h3 class="lk-title">{{__('Settings')}}
            </h3>
            <div class="white-block">
                <div class="form-section form-section--border-bottom">
                    <div class="lk-title">{{__('Add photo')}}
                    </div>
                    <div class="avatar-upload">
                        <div class="avatar-upload__image">
                            <div class="hide" id="upload-demo"></div>
                            <img class="js-save-crop-image" src="{{user()->getAvatarPath()}}" alt="">
                        </div>
                        <div class="avatar-upload__buttons">
                            <label class="avatar-upload__upload">
                                <input type="file" id="upload"><span class="btn-rhombus"><svg class="svg-icon">
                                <use href="/assets/icons/sprite.svg#icon-cross"></use>
                          </svg></span>
                            </label>
                            <button class="btn-rhombus btn-rhombus--remove js-remove-photo">
                                <svg class="svg-icon">
                                    <use href="/assets/icons/sprite.svg#icon-cross"></use>
                                </svg>
                            </button>
                            <div>
                                <button class="btn btn--green btn--size-md upload-result">
                                    <span>{{__('Save photo')}}</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>


                <form class="form-horizontal" method="POST" action="{{ route('profile.settings') }}">
                    {{ csrf_field() }}
                    <div class="form-section form-section--border-bottom">
                        <div class="lk-title">{{__('Change user data')}}
                        </div>
                        <div class="field field--row">
                            <label>{{ __('Email') }}</label>
                            <input type="email"
                                   value="{{ getUserEmail() }}" disabled>
                        </div>


                        <div class="field field--row">
                            <label>{{ __('Login') }}</label>
                            <input type="text" value="{{ getUserLogin() ? getUserLogin() : old('login') }}" disabled>
                        </div>
                        <div class="field field--row">
                            <label>{{ __('Phone') }}</label>
                            <input type="text" name="phone"
                                   value="{{ getUserPhone() ? getUserPhone() : old('phone') }}"/>
                        </div>
                    </div>
                    <div class="form-section">
                        <button class="btn btn--warning btn--size-lg">{{__('Save profile')}}
                        </button>
                    </div>
                </form>
                <form class="form-horizontal" method="POST" action="{{ route('profile.settings') }}">
                    {{ csrf_field() }}
                    <div class="form-section form-section--border-bottom">
                        <div class="lk-title">{{__('Change password')}}
                        </div>
                        <div class="field field--row">
                            <label>{{__('Password')}}</label>
                            <input type="password" name="password"/>
                        </div>
                        <div class="field field--row">
                            <label>{{__('New password')}}</label>
                            <input type="password" name="new_password"/>
                        </div>
                        <div class="field field--row">
                            <label>{{__('Re-enter password')}}</label>
                            <input type="password" name="repeat_password"/>
                        </div>
                    </div>
                    <div class="form-section">
                        <button class="btn btn--warning btn--size-lg">{{__('Save profile')}}
                        </button>
                    </div>
                </form>
                <div class="form-section form-section--border-bottom">
                    <div class="lk-title">{{__('Legal')}}
                    </div>
                    <ul class="legal-links">

                        <li><a href="{{route('customer.agreement')}}">{{__('Agreement')}}</a></li>
                    </ul>
                </div>
                <form class="form-horizontal" id="fa2_form_dis" method="POST"
                      action="{{ route('profile.settings_2fa') }}">
                    {{ csrf_field() }}
                    <div class="form-section form-section--border-bottom">
                        <div class="lk-title">{{__('2FA Authentication')}}
                        </div>
                        <div class="radio-group">
                            <label class="radio-group__button js-modal" data-modal="auth-2fa">
                                <input type="radio" name="google_2fa" id="google_2fa"
                                       value="1" {{ user()->google_2fa == 1 ? ' checked' : '' }}><span>{{__('On 2FA')}}</span>
                            </label>
                            <label class="radio-group__button" id="disable_2fa">
                                <input type="radio" name="google_2fa" id="google_2fa"
                                       value="0" {{ user()->google_2fa == 0 ? ' checked' : '' }}><span>{{__('Off 2FA')}}</span>
                            </label>
                        </div>
                    </div>
                </form>
            </div>


        </div>
    </section>

@endsection

@section('modal_content')

    <div class="modal" id="auth-2fa">
        <div class="modal-inner modal-inner--light">
            <div class="modal-inner__close js-close-modal">
                <svg class="svg-icon">
                    <use href="assets/icons/sprite.svg#icon-cross"></use>
                </svg>
            </div>
            <div class="modal-header modal-header--border-bottom">
                <h3 class="modal-title"><span>{{__('2FA Authentication')}}</span>
                </h3>
            </div>
            <form class="form-horizontal" id="fa2_form" method="POST" action="{{ route('profile.settings_2fa') }}">
                {{ csrf_field() }}
                <input type="hidden" name="google_2fa" value="1">
                <input type="hidden" name="google2fa_secret" value="{{$secret}}">
                <div class="modal-content">
                    <div class="auth-2fa">
                        <h5 class="auth-2fa__title">{{__('Set up Google Authenticator')}}
                        </h5>
                        <div class="typography">
                            <p>{{__('Set up your two factor authentication by scanning the barcode below. Alternatively, you can use the code')}} </p>
                        </div>
                        <h5 class="auth-2fa__title auth-2fa__title--ttu">{{ $secret }}
                        </h5>
                        <div class="auth-2fa__code"><img src="{{ $QR_Image }}" alt="">
                        </div>
                        <div class="typography">
                            <p>{{__('You must set up your Google Authenticator app before continuing. You will be unable to login otherwise')}}</p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer modal-footer--center">
                    <button class="btn btn--warning btn--size-lg"><span>{{__('Save security settings')}}</span>
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection

@push('load-scripts')
    <script>
        $('#disable_2fa').click(function () {
            $('#fa2_form_dis').submit();
        });

        $('.js-remove-photo').click(function () {
            $.ajax({
                type: 'get',
                url: '{{route('remove_image')}}',
                datatype: 'html',
                // data: {currency: val},
                success: function (data) {
                    $('.js-save-crop-image').attr('src', data);
                },
                error: function () {
                }
            });
        })
    </script>
@endpush