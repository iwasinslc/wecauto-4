
<section class="account-items">
    <div class="container">
        <div class="js-swiper-account swiper-container swiper-no-swiping">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <div class="account-item">
                        <div class="account-item__content">
                            <p class="account-item__title"> <strong>WEC {{__('invested')}}</strong>
                            </p>
                            <p class="account-item__value" >{{number_format(getUserTotalByTransactionType('create_dep')['WEC'], 4)}}</p>
                        </div>
                        <div class="account-item__icon"><img src="/assets/images/account-item-icons/1.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="account-item">
                        <div class="account-item__content">
                            <p class="account-item__title"> <strong>FST {{__('invested')}}</strong>
                            </p>
                            <p class="account-item__value">{{number_format(getUserTotalByTransactionType('add_fst')['WEC'], 4)}}</p>
                        </div>
                        <div class="account-item__icon"><img src="/assets/images/account-item-icons/2.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="account-item">
                        <div class="account-item__content">
                            <p class="account-item__title"> <strong>{{__('Licence')}}</strong>
                            </p>
                            <p class="account-item__value">
                                @if (user()->activeLicence())
                                    {{ user()->licence->name.' '.user()->licence->id }}
                                    <small class="color-warning"> {{user()->close_at->format('d/m/Y')}}</small>
                                @else
                                    __('No licence')
                                @endif
                            </p>
                        </div>
                        <div class="account-item__icon"><img src="/assets/images/account-item-icons/3.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="account-item">
                        <div class="account-item__content">
                            <p class="account-item__title"> <strong>{{__('Rank')}} </strong>
                            </p>
                            <p class="account-item__value">{{user()->rank->name}}
                            </p>
                        </div>
                        <div class="account-item__icon"><img src="/assets/images/account-item-icons/4.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


