<?php


namespace App\Virtual\Models;

/**
 * @OA\Schema(
 *     title="Trade",
 *     description="Trade information"
 * )
 */
class Trade
{
    /**
     * @OA\Property(
     *     title="ID",
     *     description="Trade ID",
     *     example="b5d00f20-ff1d-11ea-8e8c-211d59c5f58c"
     * )
     *
     * @var string
     */
    private $id;

    /**
     * @OA\Property(
     *     title="Type",
     *     description="Order type",
     *     example="sell"
     * )
     *
     * @var string
     */
    private $type;

    /**
     * @OA\Property(
     *     title="Value",
     *     description="Order amount",
     *     example=1000.00
     * )
     *
     * @var float
     */

    public $value;

    /**
     * @OA\Property(
     *     title="Currency",
     *     description="Currency code of the order amount",
     *     example="WEC"
     * )
     *
     * @var string
     */
    public $currency;

    /**
     * @OA\Property(
     *     title="Rate",
     *     description="Rate for current order",
     *     example=1000.00
     * )
     *
     * @var float
     */

    public $rate;

    /**
     * @OA\Property(
     *     title="Rate Currency",
     *     description="Currency code of rate",
     *     example="WEC"
     * )
     *
     * @var string
     */
    public $rate_currency;

    /**
     * @OA\Property(
     *     title="Rate amount",
     *     description="Amont converted to rate",
     *     example=1000.00
     * )
     *
     * @var float
     */
    public $rate_amount;

    /**
     * @OA\Property(
     *     title="Limit amount",
     *     description="Limit amount for trade",
     *     example=1000.00
     * )
     *
     * @var float
     */
    public $limit_amount;

    /**
     * @OA\Property(
     *     title="Limit Currency",
     *     description="Currency code of the limit for trade",
     *     example="WEC"
     * )
     *
     * @var string
     */
    public $limit_currency;

    /**
     * @OA\Property(
     *     title="Fee amount",
     *     description="Amont of the fee",
     *     example=10.00
     * )
     *
     * @var float
     */
    public $fee;

    /**
     * @OA\Property(
     *     title="Order Id",
     *     description="Order id for trade"
     * )
     *
     * @var integer
     */
    public $order_id;

    /**
     * @OA\Property(
     *     title="Created At",
     *     description="Date of the creating transaction",
     *     example="2020-01-01 17:50:45",
     *     format="datetime",
     *     type="string"
     * )
     *
     * @var \DateTime
     */
    public $created_at;

    /**
     * @OA\Property(
     *     title="Updated At",
     *     description="Date of the updating order",
     *     example="2020-01-01 17:50:45",
     *     format="datetime",
     *     type="string"
     * )
     *
     * @var \DateTime
     */
    public $updated_at;
}