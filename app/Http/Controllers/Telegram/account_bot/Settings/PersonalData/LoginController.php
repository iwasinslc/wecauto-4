<?php
namespace App\Http\Controllers\Telegram\account_bot\Settings\PersonalData;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Telegram\account_bot\Settings\PersonalData\SetupNewEmail\EnterNewEmailController;
use App\Models\Telegram\TelegramBotEvents;
use App\Models\Telegram\TelegramBotMessages;
use App\Models\Telegram\TelegramBots;
use App\Models\Telegram\TelegramBotScopes;
use App\Models\Telegram\TelegramUsers;
use App\Models\Telegram\TelegramWebhooks;
use App\Modules\Messangers\TelegramModule;

class LoginController extends Controller
{
    /**
     * @param TelegramWebhooks $webhook
     * @param TelegramBots $bot
     * @param TelegramBotScopes $scope
     * @param TelegramUsers $telegramUser
     * @param TelegramBotEvents $event
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Throwable
     */
    public function index(
        TelegramWebhooks $webhook,
                          TelegramBots $bot,
                          TelegramBotScopes $scope,
                          TelegramUsers $telegramUser,
                          TelegramBotEvents $event
    ) {
        sleep(1);
        $scope = TelegramBotScopes::where('method_address', 'like', '%LoginController%')
            ->where('bot_keyword', $bot->keyword)
            ->first();

        TelegramModule::setLanguageLocale($telegramUser->language);

        $user = $telegramUser->user;

        if (null == $user) {
            return response('ok');
        }



        $message = view('telegram.account_bot.settings.login.index', [
            'webhook'      => $webhook,
            'bot'          => $bot,
            'scope'        => $scope,
            'telegramUser' => $telegramUser,
            'event'        => $event,
        ])->render();

        if (config('app.env') == 'develop') {
            \Log::info('Prepared VIEW and message for bot:<hr>'.$message);
        }

        try {
            $telegramInstance = new TelegramModule($bot->keyword);
            $telegramInstance->sendMessage($event->chat_id, $message, 'HTML', true, false, null, null, $scope);
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response('ok');
        }

        return response('ok');
    }

    /**
     * @param TelegramWebhooks $webhook
     * @param TelegramBots $bot
     * @param TelegramBotScopes $scope
     * @param TelegramUsers $telegramUser
     * @param TelegramBotEvents $event
     * @param TelegramBotMessages $userMessage
     * @param TelegramBotMessages $botRequestMessage
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     * @throws \Throwable
     */
    public function checkAndProcessAnswer(
        TelegramWebhooks $webhook,
                                          TelegramBots $bot,
                                          TelegramBotScopes $scope,
                                          TelegramUsers $telegramUser,
                                          TelegramBotEvents $event,
                                          TelegramBotMessages $userMessage,
                                          TelegramBotMessages $botRequestMessage
    ) {
        /*
         * Validate inputs
         */
        $validator = \Validator::make([
            'login' => $userMessage->message
        ], [
            'login' => 'string|max:30|unique:users',
        ]);

        if ($validator->fails()) {
            $this->validationFailed($webhook, $bot, $scope, $telegramUser, $event, $userMessage, $botRequestMessage);
            return response('ok');
        }



        /*
         * Get and save info
         */
        /** @var User $user */
        $user = $telegramUser->user;

        if (null === $user) {
            throw new \Exception('User can not be found.');
        }

        $user->login = trim($userMessage->message);
        $user->save();

        /*
         * Answer
         */
        $this->userAnswerSuccess($webhook, $bot, $scope, $telegramUser, $event, $userMessage, $botRequestMessage);
    }

    /**
     * @param TelegramWebhooks $webhook
     * @param TelegramBots $bot
     * @param TelegramBotScopes $scope
     * @param TelegramUsers $telegramUser
     * @param TelegramBotEvents $event
     * @param TelegramBotMessages $userMessage
     * @param TelegramBotMessages $botRequestMessage
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Throwable
     */
    public function userAnswerSuccess(
        TelegramWebhooks $webhook,
                                      TelegramBots $bot,
                                      TelegramBotScopes $scope,
                                      TelegramUsers $telegramUser,
                                      TelegramBotEvents $event,
                                      TelegramBotMessages $userMessage,
                                      TelegramBotMessages $botRequestMessage
    ) {
        TelegramModule::setLanguageLocale($event->from_language_code);
        $message = view('telegram.account_bot.settings.login.user_answer_success', [
            'webhook'      => $webhook,
            'bot'          => $bot,
            'scope'        => $scope,
            'telegramUser' => $telegramUser,
            'userMessage'  => $userMessage,
        ])->render();

        if (config('app.env') == 'develop') {
            \Log::info('Prepared VIEW and message for bot:<hr>'.$message);
        }

        try {
            $telegramInstance = new TelegramModule($bot->keyword);
            $telegramInstance->sendMessage($event->chat_id, $message, 'HTML', true);
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response('ok');
        }

        TelegramBotMessages::closeUserScopes($event, $bot);

        /*
         * Initiate: Set user login
         */
        /** @var User $user */
        $user = $telegramUser->user;

        if (null == $user) {
            return response('ok');
        }

        if (password_verify('password', $user->password)) {

            app()->call(EnterNewEmailController::class.'@index', [
                'webhook' => $webhook,
                'bot' => $bot,
                'scope' => $scope,
                'telegramUser' => $telegramUser,
                'event' => $event,
            ]);
        }
    }

    /**
     * @param TelegramWebhooks $webhook
     * @param TelegramBots $bot
     * @param TelegramBotScopes $scope
     * @param TelegramUsers $telegramUser
     * @param TelegramBotEvents $event
     * @param TelegramBotMessages $userMessage
     * @param TelegramBotMessages $botRequestMessage
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Throwable
     */
    public function validationFailed(
        TelegramWebhooks $webhook,
                                     TelegramBots $bot,
                                     TelegramBotScopes $scope,
                                     TelegramUsers $telegramUser,
                                     TelegramBotEvents $event,
                                     TelegramBotMessages $userMessage,
                                     TelegramBotMessages $botRequestMessage
    ) {
        TelegramModule::setLanguageLocale($event->from_language_code);
        $message = view('telegram.account_bot.settings.login.user_answer_fails', [
            'webhook'      => $webhook,
            'bot'          => $bot,
            'scope'        => $scope,
            'telegramUser' => $telegramUser,
        ])->render();

        if (config('app.env') == 'develop') {
            \Log::info('Prepared VIEW and message for bot:<hr>'.$message);
        }

        try {
            $telegramInstance = new TelegramModule($bot->keyword);
            $telegramInstance->sendMessage($event->chat_id, $message, 'HTML', true);
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response('ok');
        }
    }

}
