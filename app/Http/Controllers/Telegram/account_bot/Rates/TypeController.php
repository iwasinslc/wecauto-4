<?php
namespace App\Http\Controllers\Telegram\account_bot\Rates;

use App\Helpers\Constants;
use App\Http\Controllers\Controller;
use App\Models\Rate;
use App\Models\Telegram\TelegramBotEvents;
use App\Models\Telegram\TelegramBots;
use App\Models\Telegram\TelegramBotScopes;
use App\Models\Telegram\TelegramUsers;
use App\Models\Telegram\TelegramWebhooks;
use App\Modules\Messangers\TelegramModule;

class TypeController extends Controller
{
    /**
     * @param TelegramWebhooks $webhook
     * @param TelegramBots $bot
     * @param TelegramBotScopes $scope
     * @param TelegramUsers $telegramUser
     * @param TelegramBotEvents $event
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     * @throws \Throwable
     */
    public function index(TelegramWebhooks $webhook,
                          TelegramBots $bot,
                          TelegramBotScopes $scope,
                          TelegramUsers $telegramUser,
                          TelegramBotEvents $event)
    {
        TelegramModule::setLanguageLocale($telegramUser->language);

        preg_match('/select\_type '.Constants::UUID_REGEX.'/', $event->text, $data);

        if (isset($data[1]))
        {
            $rate_id = $data[1];
        }
        else {
            return response('ok');
        }

        $user = $telegramUser->user;

        cache()->put('create_deposit'.$user->id, [
            'rate_id' =>$rate_id,
        ] , 30 );


        \Log::critical(print_r(cache()->get('create_deposit'.$user->id), true));

        $message = view('telegram.account_bot.rates.type', [
            'webhook'      => $webhook,
            'bot'          => $bot,
            'scope'        => $scope,
            'telegramUser' => $telegramUser,
            'event'        => $event,
        ])->render();

        if (config('app.env') == 'develop') {
            \Log::info('Prepared VIEW and message for bot:<hr>'.$message);
        }



        $keyboard = [
            [
                ['text' => __('With payment system'), 'callback_data' => 'create_deposit 1'],
            ],
            [

                ['text' => __('From account balance'), 'callback_data' => 'create_deposit 2'],
            ],
        ];



        try {
            $telegramInstance = new TelegramModule($bot->keyword);
            $telegramInstance->sendMessage($event->chat_id,
                $message,
                'HTML',
                true,
                false,
                null,
                [
                    'inline_keyboard' => $keyboard,
                    'resize_keyboard' => true,
                    'one_time_keyboard' => true,
                ],
                $scope,
                'inline_keyboard');
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response('ok');
        }





        return response('ok');
    }
}