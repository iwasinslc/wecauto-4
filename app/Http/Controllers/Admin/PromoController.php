<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Currency;
use App\Models\TransactionType;
use App\Models\User;
use App\Models\Wallet;
use Yajra\DataTables\DataTables;

/**
 * Class PromoController
 * @package App\Http\Controllers\Admin
 */
class PromoController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function usd()
    {


        return view('admin.promo.usd', [
//            'users'=>$array
        ]);
    }


    public function acc()
    {


        return view('admin.promo.acc', [
//            'users'=>$array
        ]);
    }


    public function ddata_usd()
    {
        $wallets = Wallet::where('currency_id', Currency::getByCode('USD')->id)
            ->where('balance', '>', 0)
            ->with(['user']);

        return Datatables::of($wallets)->addColumn('show', function ($wallet) {
            return route('admin.users.show', ['user' => $wallet->user->id]);
        })->make(true);
    }

    public function ddata_acc()
    {
        $wallets = Wallet::where('currency_id', Currency::getByCode('ACC')->id)
            ->where('balance', '>', 0)
            ->with(['user']);

        return Datatables::of($wallets)->addColumn('show', function ($wallet) {
            return route('admin.users.show', ['user' => $wallet->user->id]);
        })->make(true);
    }
}
