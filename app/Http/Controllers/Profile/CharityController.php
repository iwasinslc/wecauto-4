<?php
namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Http\Requests\RequestCharity;
use App\Http\Requests\RequestCreateDeposit;
use App\Models\Deposit;
use App\Models\Rate;
use App\Models\Transaction;
use App\Models\TransactionType;
use App\Models\User;
use App\Models\Wallet;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class CharityController extends Controller
{

    public function index()
    {

        if (!\user()->hasRole(['root']))
        {
            return response('ok');
        }

        return view('profile.charity');
    }

    /**
     * @param RequestCreateDeposit $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function store(RequestCharity $request)
    {

        if (!user()->hasRole(['root']))
        {
            return back()->with('error', __('Function dont working'));
        }


        $data = cache()->pull('protect-exchange-'.getUserId());

        if ($data!==null)
        {

            return back()->with('error', __('Error'));
        }

        cache()->put('protect-exchange-'.getUserId(), '1', 0.1);


        $wallet = user()->wallets()->find($request->wallet_id);

        if ($wallet->balance - $wallet->balance * TransactionType::getByName('withdraw')->commission * 0.01 < $request->amount) {
            return back()->with('error', __('Requested amount exceeds the wallet balance'));
        }


        try {
            Transaction::charity($wallet, $request->amount);
        } catch(\Exception $e) {
            return back()->with('error', $e->getMessage());
        }

        return back()->with('success', __('Thank for charity'));
    }


}
