<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestCashbackRequestStore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'telegram' => 'required|string|max:100',
            'video_link' => ['required', 'url', 'regex:/^(https:\/\/drive\.google\.com\/file\/d\/(.*?)\/.*?\?usp=sharing)|(https:\/\/yadi\.sk\/(.*?))$/i'],
            'comment' => 'required|string|max:1000',
            'confirm' => 'required',
            'files' => 'required',
            'files.*' => 'file|mimes:pdf,tiff,png,jpg,jpeg' //
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'files.required' => __('At least one file must be attached'),
            'video_link.required' => __('Link to the video is required'),
            'video_link.url' => __('Link to the video must be correct url'),
            'confirm.required' => __('Confirmation checkbox is required'),
            'video_link.regex' => __('Link must be to Google or Yandex drivers')
        ];
    }
}
