<?php
namespace App\Rules;

use App\Models\ExchangeOrder;
use App\Models\Wallet;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Auth;

/**
 * Class RuleHasPhone
 * @package App\Rules
 */
class RuleLicenceLimits implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {

        /**
         * @var Wallet $mainWallet
         */
        $mainWallet = user()->wallets()->find(request()->main_wallet_id);


        if ($mainWallet->currency->code!='FST') return true;

        /**
         * @var Wallet $wallet
         */
        $wallet = user()->wallets()->find(request()->wallet_id);


        $amount = $value*request()->rate*rate($wallet->currency->code, 'USD');





        $limit = ExchangeOrder::TYPE_SELL==request()->type ? user()->sellLimit() : user()->buyLimit();

        return $limit>=$amount;

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('Limits is done');
    }
}
