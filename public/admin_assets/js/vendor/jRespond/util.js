/*
 * Copyright. "Hyipium" engine. All rights reserved.
 * Any questions? Please, visit https://hyipium.com
 */

// make console.log safe to use
window.console||(console={log:function(){}});