<?php
namespace Database\Seeders;

use App\Models\TransactionType;
use Illuminate\Database\Seeder;

/**
 * Class TransactionTypesSeeder
 */
class TransactionTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $transactionTypes = [
            'enter',
            'withdraw',
            'bonus',
            'partner',
            'dividend',
            'create_dep',
            'close_dep',
            'penalty',


            'buy_license',
            'exchange_order',
            'exchange_close_order',
            'charity',

            'exchange_commission',
            'cashback',
            'enter_exchange',
            'order_stash',
            'licence_cash_back',
            'transfer_send',
            'transfer_receive',
            'add_fst',
            'set_rank'
        ];

        foreach ($transactionTypes as $type) {
            $searchType = TransactionType::where('name', $type)->count();

            if ($searchType > 0) {
                echo "Transaction type '".$type."' already registered.\n";
                continue;
            }

            TransactionType::create([
                'name'       => $type,
                'commission' => 0,
            ]);
            echo "Transaction type '".$type."' registered.\n";
        }
    }
}
