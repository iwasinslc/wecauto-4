<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Projects extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->string('id')->primary();

            $table->string('name')->nullable();
            $table->string('logotype_url')->nullable();
            $table->string('curator_contacts')->nullable();
            $table->string('url')->nullable();
            $table->longText('description')->nullable();
            $table->string('video_description')->nullable();
            $table->boolean('published')->default(0);
            $table->boolean('verified')->default(0);
            $table->string('user_id')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
